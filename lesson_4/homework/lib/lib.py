import pickle
from itertools import zip_longest


def convert_list_to_tuple(my_list, needle):
    """
    1. Функция convert_list_to_tuple. Принимает 2 аргумента: список (с повторяющимися элементами) и
    значение для поиска. Возвращает кортеж (из входного списка) и найдено ли искомое значение(True |
    False).
    """
    return tuple(my_list), True if needle in my_list else False


def convert_list_to_set(my_list):
    """
    2. Функция convert_list_to_set. Принимает 1 аргумент: список (с повторяющимися элементами).
    Возвращает множество (из входного списка) и количество элементов в множестве.
    """
    my_set = set(my_list)
    return my_set, len(my_set)


def convert_list_to_dict(my_list, needle):
    """
    3. Функция convert_list_to_dict. Принимает 2 аргумента: список (с повторяющимися элементами) и
    значение для поиска. Возвращает словарь (из входного списка), в котором ключ=значение из списка, а
    значение=индекс этого элемента в списке, найдено ли искомое значение(True | False) в ключах
    словаря и найдено ли искомое значение(True | False) в значениях словаря, количество элементов в
    словаре.
    """
    my_dict = {v: i for i, v in enumerate(my_list)}
    # Сначала было так, не понравилось:
    # for index, val in enumerate(my_list, 0):
    #     my_dict[val] = index

    return [
        "Dictionary: " + str(my_dict),
        "Found in keys: " + str(needle in my_dict.keys()),
        "Found in values: " + str(needle in my_dict.values())
    ]


def convert_list_to_str(my_list: list, delimiter: str):
    """
    4. Функция convert_list_to_str. Принимает 2 аргумента: список (с повторяющимися элементами) и
    разделитель (строка). Возвращает строку полученную разделением элементов списка разделителем,
    а также количество разделителей в получившейся строке в квадрате.
    """
    my_str = delimiter.join(str(val) for val in my_list)
    length = (len(my_str.split(delimiter)) - 1) ** 2
    return my_str, length


def convert_tuple_to_list(my_tuple, needle):
    """
    5. Функция convert_tuple_to_list. Принимает 2 аргумента: кортеж (с повторяющимися элементами) и
    значение для поиска. Возвращает список (из входного кортежа) и найдено ли искомое значение (True
    | False).
    """
    return list(my_tuple), True if my_tuple.count(needle) > 0 else False


def convert_tuple_to_set(my_tuple):
    """
    6. Функция convert_tuple_to_set. Принимает 1 аргумент: кортеж (с повторяющимися элементами).
    Возвращает множество (из входного кортежа) и количество элементов в множестве.
    """
    my_set = set(my_tuple)
    return my_set, len(my_set)


def convert_tuple_to_dict(my_tuple, needle):
    """
    7. Функция convert_tuple_to_dict. Принимает 2 аргумента: кортеж (с повторяющимися элементами) и
    значение для поиска. Возвращает словарь (из входного кортежа), в котором ключ=значение из
    кортежа, а значение=индекс этого элемента в кортеже, а также найдено ли искомое значение(True |
    False) в ключах словаря и найдено ли искомое значение(True | False) в значениях словаря.
    """
    my_dict = {v: i for i, v in enumerate(my_tuple)}
    return my_dict, needle in my_dict.keys(), needle in my_dict.values()


def convert_tuple_to_str(my_tuple, delimiter):
    """
    Функция convert_tuple_to_str. Принимает 2 аргумента: кортеж (с повторяющимися элементами) и
    разделитель (строка). Возвращает строку полученную разделением элементов кортежа
    разделителем, а также количество разделителей в получившейся строке в степени 3.
    """
    my_str = delimiter.join(str(x) for x in my_tuple)
    return my_str, (my_str.count(delimiter)) ** 3


def convert_set_to_list(my_set: set, needle: int):
    """
    9. Функция convert_set_to_list. Принимает 2 аргумента: множество и значение для поиска. Возвращает
    список (из входного множества), а также True, если искомое значение НЕ найдено в списке, иначе
    False.
    """
    return list(my_set), True if needle not in my_set else False


def convert_set_to_tuple(my_set: set, needle: int):
    """
    10. Функция convert_set_to_tuple. Принимает 2 аргумента: множество и значение для поиска.
    Возвращает список (из входного множества) повторенный 2 раза, а также True, если искомое
    значение найдено в списке, иначе False.
    """
    return [[x for x in my_set] for not_used in range(2)], True if list(my_set).count(needle) > 0 else False


def convert_set_to_dict(my_set: set, needle: int):
    """
    11. Функция convert_set_to_dict. Принимает 2 аргумента: множество и значение для поиска. Возвращает
    словарь (из входного множества), в котором ключ=значение из множества, а значение=индекс этого
    элемента в множестве в квадрате, найдено ли искомое значение(True | False) в ключах словаря и
    найдено ли искомое значение(True | False) в значениях словаря, количество элементов в словаре.
    """
    my_dict = {v: i ** 2 for i, v in enumerate(my_set)}
    return my_dict, needle in my_dict.keys(), needle in my_dict.values(), len(my_dict) + len(my_dict.values())


def convert_set_to_str(my_set: set, delimiter: str):
    """
    12. Функция convert_set_to_str. Принимает 2 аргумента: множество и разделитель (строка). Возвращает
    строку полученную разделением элементов множества разделителем, а также количество
    разделителей в получившейся строке в степени 5.
    """
    my_str = delimiter.join(str(x) for x in my_set)
    return my_str, my_str.count(delimiter) ** 5


def convert_dict_to_list(my_dict: dict):
    """
    13. Функция convert_dict_to_list. Принимает 1 аргумент: словарь (в котором значения повторяются).
    Возвращает список ключей, список значений, количество элементов в списке ключей, количество
    элементов в списке значений.
    """
    return list(my_dict.keys()), list(my_dict.values()), len(my_dict.keys()), len(my_dict.values())


def convert_dict_to_tuple(my_dict: dict):
    """
    14. Функция convert_dict_to_tuple. Принимает 1 аргумент: словарь (в котором значения повторяются).
    Возвращает кортеж ключей, множество значений, а также True, если хотя бы один ключ равен одному
    из значений.
    """
    return tuple(my_dict.keys()), set(my_dict.values()), \
           len(set(my_dict.keys()).intersection(my_dict.values())) > 0  # TODO Это треш, что делать?


def convert_dict_to_set(my_dict: dict):
    """
    15. Функция convert_dict_to_set. Принимает 1 аргумент: словарь (в котором значения повторяются).
    Возвращает множество ключей, множество значений, количество элементов в множестве ключей,
    количество элементов в множестве значений.
    """
    return set(my_dict.keys()), set(my_dict.values()), len(set(my_dict.keys())), len(set(my_dict.values()))


def convert_dict_to_str(my_dict: dict):
    """
    16. Функция convert_dict_to_str. Принимает 1 аргумент: словарь (в котором значения повторяются).
    Возвращает строку вида “key1=val1 | key2 = val2 | key3 = val3”. (* Подсказка: помним про конкатенацию
    строк или zip).
    """
    return " | ".join([str(k) + "=" + str(v) for k, v in my_dict.items()])


def zip_names(names: list, last_names: set):
    """
    1. Функция zip_names. Принимает 2 аргумента: список с именами и множество с фамилиями.
    Возвращает список с парами значений из каждого аргумента. (* Провести эксперименты с различными
    длинами входных данных. Если они одинаковые? А если разные? А если один из них пустой?)
    """
    return list(zip(names, last_names))


def zip_colour_shape(colors: list, shapes: tuple):
    """
    2. Функция zip_colour_shape. Принимает 2 аргумента: список с цветами(зеленый, красный) и кортеж с
    формами (квадрат, круг). Возвращает список с парами значений из каждого аргумента. (* Провести
    эксперименты с различными длинами входных данных. Если они одинаковые? А если разные? А если
    один из них пустой?)
    """
    # TODO А как мне сделать все возможные пары, это возможно через zip?
    return list(zip(colors, shapes))


def zip_car_year(cars: list, years: list):
    """
    3. Функция zip_car_year. Принимает 2 аргумента: список с машинами и список с годами производства.
    Возвращает список с парами значений из каждого аргумента, если один список больше другого, то
    заполнить недостающие элементы строкой “???”. (* Подсказка: zip_longest. Провести эксперименты с
    различными длинами входных данных. Если они одинаковые? А если разные? А если один из них
    пустой?)
    """
    return list(zip_longest(cars, years, fillvalue='???'))


def gen_list_double(n: int):
    """
    Функция gen_list_double. Принимает число n. Возвращает список длиной n, состоящий из удвоенных
    значений от 0 до n. Пример: n=3, результат [0, 2, 4].
    """
    return [x*2 for x in range(n)]


def gen_list_double_start_stop(start: int, stop: int):
    """
    2. Функция gen_list_double_start_stop. Принимает числа start, stop. Возвращает список состоящий из
    удвоенных значений от start до stop (не включая). Пример: start=3, stop=6, результат [6, 8, 10].
    """
    return [x*2 for x in range(start, stop)]


def gen_list_pow_start_stop(start: int, stop: int):
    """
    3. Функция gen_list_pow_start_stop. Принимает числа start, stop. Возвращает список состоящий из
    квадратов значений от start до stop (не включая). Пример: start=3, stop=6, результат [9, 16, 25].
    """
    return [x**2 for x in range(start, stop)]


def gen_list_pow_even(n: int) -> list:
    """
    4. Функция gen_list_pow_even. Принимает число n. Возвращает список длиной n, состоящий из
    квадратов четных чисел в диапазоне от 0 до n. Пример: n = 8, четные числа [0, 2, 4, 6], результат [0, 4,
    16, 36].
    """
    return [x**2 for x in range(0, n) if x % 2 == 0]


def gen_list_pow_odd(n: int) -> list:
    """
    5. Функция gen_list_pow_odd. Принимает число n. Возвращает список длиной n, состоящий из
    квадратов нечетных чисел в диапазоне от 0 до n. Пример: n = 7, четные числа [1, 3, 5], результат [1, 9, 25].
    """
    return [x**2 for x in range(0, n) if x % 2 != 0]


def gen_dict_double(n: int) -> dict:
    """
    1. Функция gen_dict_double. Принимает число n. Возвращает словарь длиной n, в котором ключ - это
    значение от 0 до n, а значение - удвоенное значение ключа. Пример: n=3, результат {0: 0, 1: 2, 2: 4}.
    """
    return {x: x*2 for x in range(n)}


def gen_dict_double_start_stop(start: int, stop: int) -> dict:
    """
    2. Функция gen_dict_double_start_stop. Принимает числа start, stop. Возвращает словарь, в котором
    ключ - это значение от start до stop (не включая), а значение - удвоенных значение ключа. Пример:
    start=3, stop=6, результат {3: 6, 4: 8, 5: 10}.
    """
    return {x: x*2 for x in range(start, stop)}


def gen_dict_pow_start_stop(start: int, stop: int) -> dict:
    """
    3. Функция gen_dict_pow_start_stop. Принимает числа start, stop. Возвращает словарь, в котором ключ
    - это значение от start до stop (не включая), а значение - это квадрат ключа . Пример: start=3, stop=6,
    результат {3: 9, 4: 16, 5: 25}.
    """
    return {x: x**2 for x in range(start, stop)}


def gen_dict_pow_even(n: int) -> dict:
    """
    4. Функция gen_dict_pow_even. Принимает число n. Возвращает словарь длиной n, в котором ключ - это
    четное число в диапазоне от 0 до n, а значение - квадрат ключа. Пример: n = 8, четные числа [0, 2, 4, 6],
    результат {0: 0, 2: 4, 4: 16, 6: 36}.
    """
    return {x: x**2 for x in range(n) if x % 2 == 0}


def gen_dict_pow_odd(n: int) -> dict:
    """
    5. Функция gen_dict_pow_odd. Принимает число n. Возвращает словарь длиной n, в котором ключ - это
    нечетное число в диапазоне от 0 до n, а значение - квадрат ключа. Пример: n = 7, четные числа [1, 3,
    5], результат {1: 1, 3: 9, 5: 25}.
    """
    return {x: x**2 for x in range(n) if x % 2 != 0}


def save_list_to_file_classic():
    """
    1. Функция save_list_to_file_classic. Принимает 2 аргумента: строка (название файла или полный путь к
    файлу), список (для сохранения). Сохраняет список в файл. Проверить, что записалось в файл.
    """
    file = open("test1.txt", "w")
    file.write(str([1, 2, 3, 4]))
    file.close()


def save_list_to_file_pickle():
    """
    2. Функция save_list_to_file_pickle. Принимает 2 аргумента: строка (название файла или полный путь к
    файлу), список (для сохранения). Сохраняет список в файл. Загрузить список и проверить его на
    корректность.
    """
    my_list = ["file", 1, 2, 3, 4]

    # with open("test2.txt", "w") as file:
    #     file.write(str(my_list))
    #
    # with open("test2.txt", "r") as file:
    #     data = file.readline()
    #
    # if str(my_list) == data:
    #     return "Correct"
    #
    # return "Incorrect"

    with open("test2.pkl", "wb") as file:
        pickle.dump(my_list, file)

    with open("test2.pkl", "rb") as file:
        new_list = pickle.load(file)

    if my_list == new_list:
        return "Correct"

    return "Incorrect"


def save_dict_to_file_classic():
    """
    1. Функция save_list_to_file_classic. Принимает 2 аргумента: строка (название файла или полный путь к
    файлу), список (для сохранения). Сохраняет список в файл. Проверить, что записалось в файл.
    """
    file = open("test3.txt", "w")
    file.write(str(
        {
            "key1": 1,
            "key2": 2,
            "key3": 3,
        }
    ))
    file.close()


def save_dict_to_file_pickle():
    """
    4. Функция save_dict_to_file_pickle. Принимает 2 аргумента: строка (название файла или полный путь к
    файлу), словарь (для сохранения). Сохраняет список в файл. Загрузить словарь и проверить его на
    корректность.
    """
    my_dist = {1: 1, 2: 2, 3: 145}

    with open("test4.pkl", "wb") as file:
        pickle.dump(my_dist, file)

    with open("test4.pkl", "rb") as file:
        new_list = pickle.load(file)

    if my_dist == new_list:
        return "Correct"

    return "Incorrect"


def read_str_from_file():
    """
    5. Функция read_str_from_file. Принимает 1 аргумент: строка (название файла или полный путь к
    файлу). Выводит содержимое файла (в консоль). (* Файл должен быть предварительно создан и
    наполнен каким-то текстом.)
    """
    with open("test5.txt") as f:
        s = f.read()

    return s
