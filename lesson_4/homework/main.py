from lesson_4.homework.lib.lib import *


def main():
    print(convert_list_to_tuple([1, 2, 2, 3], 2))
    print(convert_list_to_set([1, 2, 3, 4, 4, True, False, 5]))
    print(convert_list_to_dict([1, 2, 3, 4, 5], 5))
    print(convert_list_to_str([1, 2, 3, 4, 6, 7, 8], "|"))
    print(convert_tuple_to_list((2, 3, 3, 1, 6, 55, 6), 11))
    print(convert_tuple_to_set((1, 2, 2, 3, 3, 4, 5, 5, 6)))
    print(convert_tuple_to_dict((1, 2, 3, 4), 3))
    print(convert_tuple_to_str((1, 2, 3, 4), "."))
    print(convert_set_to_list({1, 2, 3, 4}, 2))
    print(convert_set_to_tuple({4, 5, 6, 7}, 5))
    print(convert_set_to_dict({1, 2, 3, 4}, 9))
    print(convert_set_to_str({1, 2, 3}, "!"))
    print(convert_dict_to_list({1: 2, 3: 4, 5: 6}))
    print(convert_dict_to_tuple({1: 2, 3: 4, 5: 1}))
    print(convert_dict_to_set({1: 2, 3: 4, 5: 1}))
    print(convert_dict_to_str({"one": 1, "two": 2, "three": 3}))

    # Zip
    # вывод: не надо в зип пихать лист и сет, может быть непредсказуемый результат
    print(zip_names(["dima", "kolya"], {"atata", "test", "another"}))
    print(zip_colour_shape(["red", "green"], ("square", "triangle", "circle")))
    print(zip_car_year(['car1', 'car2', 'car3', 'car4'], ['1990', '1991', '1992']))

    # List Comprehension
    print(gen_list_double(3))
    print(gen_list_double_start_stop(3, 8))
    print(gen_list_pow_start_stop(3, 8))
    print(gen_list_pow_even(8))
    print(gen_list_pow_odd(8))

    # Dict Comprehension
    print(gen_dict_double(3))
    print(gen_dict_double_start_stop(3, 6))
    print(gen_dict_pow_start_stop(3, 6))
    print(gen_dict_pow_even(8))
    print(gen_dict_pow_odd(7))

    # Files
    save_list_to_file_classic()
    print(save_list_to_file_pickle())
    save_dict_to_file_classic()
    print(save_dict_to_file_pickle())
    print(read_str_from_file())

if __name__ == '__main__':
    main()

