def list_count_num(my_list, num):
    return my_list.count(num)


def list_count_word(my_list, num):
    return my_list.count(num)


def list_count_word_num(my_list):
    numbers = 0
    words = 0

    for elem in my_list:
        if isinstance(elem, str):
            words += 1

        if isinstance(elem, int):
            numbers += 1

    return numbers, words


def list_if_found(my_list, num, word):
    if word in my_list:
        return "I found {}".format(word)

    if num in my_list:
        return "I found {}".format(num)

    if num in my_list and word in my_list:
        return "I found {word} and {num}".format(word=word, num=num)

    if num not in my_list and word not in my_list:
        return "Sorry, I can’t find anything..."


def list_magic_parts(my_list):
    """
    Функция list_magic_parts. Принимает 1 аргумент: список “ассорти” my_list (который создал
    пользователь). Возвращает список, который состоит из [первые 2 элемента my_list] + [последний
    элемент my_list] + [количество элементов в списке my_list]. Пример: входной список [1, 2, ‘aa’, ‘mm’],
    результат [1, 2, ‘mm’, 4]. (* Проверить существуют ли “первые 2”, если есть только 1, то берем его.
    Если список пустой - вернуть пустой список. Нумерация элементов начинается с 0.)
    """
    if len(my_list) <= 0:
        return []

    return [my_list[0:2], my_list[-1], len(my_list)]


def list_magic_mul(my_list):
    """
    6. Функция list_magic_mul. Принимает 1 аргумент: список “ассорти” my_list (который создал
    пользователь). Возвращает список, который состоит из [первого элемента my_list] + [три раза
    повторенных списков my_list] + [последнего элемента my_list]. Пример: входной список [1, ‘aa’, 99],
    результат [1, 1, ‘aa’, 99, 1, ‘aa’, 99, 1, ‘aa’, 99, 99]. (* Если индекс выходит за пределы списка, то
    последний. Если список пустой - вернуть пустой список. Нумерация элементов начинается с 0.)
    """
    return [my_list[0]] + my_list * 3 + [my_list[-1]]


def list_magic_reversel(my_list):
    """
    7. Функция list_magic_reversel. Принимает 1 аргумент: список “ассорти” my_list (который создал
    пользователь). Создает новый список new_list (копия my_list), порядок которого обратный my_list.
    Возвращает список, который состоит из [второго элемента new_list] + [предпоследнего элемента
    new_list] + [весь new_list]. Пример: входной список [1, ‘aa’, 99], new_list [99, ‘aa’, 1], результат [1, 1, 99,
    ‘aa’, 1]. (* Если индекс выходит за пределы списка, то последний. Если список пустой - вернуть пустой
    список. Нумерация элементов начинается с 0.)
    """
    new_list = my_list.copy()
    new_list.reverse()
    result = new_list[1:2] + new_list[-2:-2] + new_list
    return result


def tuple_len_first(my_tuple):
    """
    1. Функция tuple_len_first. Принимает 1 аргумент: кортеж “ассорти” my_tuple (который создал
    пользователь). Возвращает кортеж состоящий из длины кортежа и первого элемента кортежа.
    Пример: (‘55’, ‘aa’, 66), результат (3, ‘55’).
    """
    return len(my_tuple), my_tuple[0]


def tuple_len_count_first(my_tuple, num):
    """
    2. Функция tuple_len_count_first. Принимает 2 аргумента: кортеж “ассорти” my_tuple (который создал
    пользователь) и число num. Возвращает кортеж состоящий из длины кортежа, количества чисел num
    в кортеже my tuple и первого элемента кортежа. Пример: my_tuple=(‘55’, ‘aa’, 66) num = 66, результат
    (3, 1, ‘55’).
    """

    return len(my_tuple), my_tuple.count(num), my_tuple[0]


def set_print_info_for_2(my_set_left: set, my_set_right: set):
    """
    1. Функция set_print_info_for_2. Принимает 2 аргумента: множества “ассорти” my_set_left и my_set_right
    (которые создал пользователь). Выводит информацию о: равенстве множеств, имеют ли они общие
    элементы, является ли my_set_left подмножеством my_set_right и наоборот.
    """
    return [
        "Is disjoint: " + str(my_set_left.isdisjoint(my_set_right)),
        "Intersection: " + ("False" if len(my_set_left.intersection(my_set_right)) <= 0 else "True"),
        "Left is subset of right: " + str(my_set_left.issubset(my_set_right)),
        "Right is subset of left: " + str(my_set_right.issubset(my_set_left)),
        "Union of sets: " + str(my_set_left.union(my_set_right)),
        "Diff: " + str(my_set_left.difference(my_set_right))
    ]


# TODO Как это правильно решить?
def set_print_info_for_3(my_set_left: set, my_set_mid: set, my_set_right: set):
    """
    2. Функция set_print_info_for_3. Принимает 3 аргумента: множества “ассорти” my_set_left, my_set_mid и
    my_set_right (которые создал пользователь). Выводит информацию о: равенстве множеств, имеют ли
    они общие элементы, являются ли они подмножествами друг друга.
    """
    are_equals = False
    if my_set_left.issubset(my_set_mid) and my_set_mid.issubset(my_set_left)\
            and my_set_left.issubset(my_set_right) and my_set_right.issubset(my_set_left)\
            and my_set_mid.issubset(my_set_right) and my_set_right.issubset(my_set_mid):
        are_equals = True

    are_intersection = False
    if len(my_set_left.intersection(my_set_mid)) > 0\
            or len(my_set_left.intersection(my_set_right)) > 0 or \
            len(my_set_mid.intersection(my_set_right)) > 0:
        are_intersection = True

    are_subsets = False
    if my_set_left.issubset(my_set_mid) or my_set_mid.issubset(my_set_left)\
            or my_set_left.issubset(my_set_right) or my_set_right.issubset(my_set_left)\
            or my_set_mid.issubset(my_set_right) or my_set_right.issubset(my_set_mid):
        are_subsets = True

    return {
        "Are equals: " + str(are_equals),
        "Are intersections: " + str(are_intersection),
        "Are subsets: " + str(are_subsets)
    }


def get_translation_by_word(my_dict: dict, word: str):
    """
    3. Функция get_translation_by_word. Принимает 2 аргумента: ru-eng словарь содержащий ru_word:
    [eng_1, eng_2 ...] и слово для поиска в словаре (ru). Возвращает все варианты переводов, если такое
    слово есть в словаре, если нет, то ‘Can’t find Russian word: {word}’.
    """
    if word not in my_dict.keys():
        return "Can't find russian word: {word}".format(word=word)

    return my_dict[word]


def get_words_by_translation(my_dict: dict, word: str):
    """
    4. Функция get_words_by_translation. ru-eng словарь содержащий ru_word: [eng_1, eng_2 ...] и слово
    для поиска в словаре (eng). Возвращает список всех вариантов переводов (ru), если такое слово есть
    в словаре (eng), если нет, то ‘Can’t find English word: {word}
    """

    # TODO Как это сделать через генераторы или как-то еще?
    #   Задача: найти слово в value словаря, при том, что сами value - это списки
    values = []
    for x in my_dict.values():
        values += x

    if word not in values:
        return "Can't find english word: {word}".format(word=word)

    for k, v in my_dict.items():
        if word in v:
            return k

    raise Exception("Something gone wrong in here")

