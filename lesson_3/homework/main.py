from lesson_3.homework.lib.lib import *


def main():
    print("list_count_num: {count}".format(count=list_count_num([1, 2, 3, 2, 3, 4, 3, 5], 2)))

    print("list_count_word: {count}".format(count=list_count_word(['test', 'test', 'test2'], 'test')))

    words, numbers = list_count_word_num(["word1", 333, "word2", 666, "word3"])
    print(f"{words}, {numbers}")

    print(list_if_found([1, 2, 'word', 'test'], 9, 'oh'))

    print(list_magic_parts([1, 2, 3, 4, 5, 100, 200]))

    print(list_magic_mul([1, 'aa', 99]))

    print(list_magic_reversel([1]))

    print(tuple_len_first(('aa', 2, 3, 4)))

    print(tuple_len_count_first((1, 2, 4, 5, 6, 7, 5, 5, 5), 5))

    print(set_print_info_for_2({1, 2, 3}, {3, 4, 5}))

    print(set_print_info_for_3({1, 2}, {9}, {5}))

    print(get_translation_by_word({
        "hi": ["здрасьти", "oh shit here we go again"],
        "hello": ["привет", "oh shit here we go again"]
    }, "hello"))

    print(get_words_by_translation(
        {
            "ой": ["ouch", "daaah"],
            "привет": ["hi", "hello"],
            "питон": ["python", "IMA MEGA SNAEK!!11"],
        }, "привет"
    ))


if __name__ == '__main__':
    main()

