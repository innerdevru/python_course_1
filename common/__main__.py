def main():
    """
    Файл для тестирования всякой фигни.
    """

    md = {"unmutable_key": 5, "unmutable_key2": 6}
    print(*md) # TODO What the fuck?..

    dict1 = {'a': 1, 'b': 2}
    dict2 = {'c': 3, 'd': 4}
    dict3 = {**dict1, **dict2}
    print(dict3)

    dict1 = {'a': 1, 'b': 2}
    dict2 = {'c': 3, 'd': 4}
    dict3 = dict1.copy()
    dict3.update(dict2) # TODO What is this?..
    print(dict3)        	# {'a': 1, 'c': 3, 'b': 2, 'd': 4}

    a = {1, 2, 3}
    b = {4, 5, 6}
    c = {"a", "b"}
    print(a & b) # intersection
    print(a | b) # concatenation
    print(set.intersection(a, b))
    print("set.difference: {}".format(set.difference(a, b, c))) # TODO WTF?! How can we diff 3 sets?
    print(set.symmetric_difference(a, b))


if __name__ == "__main__":
    main()