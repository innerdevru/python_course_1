import unittest
import random
from typing import List


def season(month_number: int) -> str:
    """
    1. Функция season. Принимает 1 аргумент — номер месяца (от 1 до 12), и возвращает время года, которому этот месяц
    принадлежит (зима, весна, лето или осень).
    """
    seasons = {
        # TODO Словарь в Python - это HashMap? Какие типы могут быть ключами, если в Python всё - объект?
        "Winter": [12, 1, 2],
        "Spring": [3, 4, 5],
        "Summer": [6, 7, 8],
        "Autumn": [9, 10, 11],
    }

    if month_number < 1 or month_number > 12:
        raise Exception("Month number can't be less than 1 or greater than 12")

    # TODO Можно ли это как-нибудь упростить в Python?
    for season_name, season_months in seasons.items():
        if month_number in season_months:
            return season_name

    # TODO Нормально ли кинуть исключение вместо return, если я хотел бы "вернуть" именно ошибку?
    raise Exception("Season not found")


# TODO Можно ли каким-то образом передать в функцию значения "по ссылке" (что-то вроде "def swap(&a, &b)")?
#  Для того, чтобы результат свапа был "виден" и вне этой функции (значения переменных и вне функции)?
def swap_v1(a: int, b: int) -> None:
    """
    2. Функция swap. Принимает 2 переменных с некоторыми значениями. Поменять местами значения этих переменных
    и вывести на экран (например, “a = 3 | b = True”).
    """
    print("Swap v1: a == {}, b == {} => ".format(a, b), end='')
    a, b = b, a
    print("a == {}, b == {}".format(a, b))


def swap_v2(values: List) -> List:
    """
    Функция swap, вариант 2.
    """
    values[0], values[1] = values[1], values[0]
    return values


def swap_v3(*args: int) -> List:
    """
    Функция swap, вариант 3.
    """
    if len(args) != 2:
        raise Exception("swap_v3 can process only 2 arguments")
    return [args[1], args[0]]


def swap_v4(a: int, b: int) -> List:
    """
    Функция swap, вариант 4. Смысл в ее вызове (см. __main__.py), а не в самой функции.
    """
    return [b, a]


# TODO Можно ли как-то указать, что функция может вернуть bool, а может выкинуть исключение?
# TODO Правильно ли для *args указывать тип как просто int, может быть нужен List[int] или что-то другое?
def check_numbers_by_5(*args: int) -> bool:
    """
    3. Функция check_numbers_by_5. Принимает 3 числа. Если ровно два из них меньше 5, то вернуть True,
    иначе вернуть False.
    """
    if len(args) != 3:
        raise Exception("Function can process only 3 arguments")

    count_of_arguments_less_5 = 0
    for x in args:
        if x < 5:
            count_of_arguments_less_5 += 1

    if count_of_arguments_less_5 == 2:
        return True

    return False


def check_equal(a: int, b: int, c: int) -> bool:
    """
    4. Функция check_equal. Принимает 3 числа. Вернуть True, если среди них есть одинаковые, иначе False
    """
    if a == b or a == c or b == c:
        return True
    return False


def count_positive(a: int, b: int, c: int) -> int:
    """
    5. Функция count_positive. Принимает 3 числа. Вернуть количество положительных чисел среди них
    """
    # Let's keep it simple for now
    positive = 0
    if a > 0:
        positive += 1
    if b > 0:
        positive += 1
    if c > 0:
        positive += 1
    return positive


# TODO Можно ли указывать возвращаемый тип вот так "int or str"? Не удалось загуглить.
def sum_div_5(a: int, b, c) -> int or str:
    """
    6. Функция sum_div_5. Принимает 3 числа. Найти сумму тех чисел, которые делятся на 5. Если таких чисел нет,
    то вывести error.
    TODO Вообще-то любое число делится на пять :) Наверное, имелось ввиду "без остатка".
    """
    summary = 0

    if a % 5 != 0 and b % 5 != 0 and c % 5 != 0:
        return "error"

    if a % 5 == 0:
        summary += a

    if b % 5 == 0:
        summary += b

    if c % 5 == 0:
        summary += c

    return summary


def if_7_yes_no(number: int) -> str:
    """
    7. Функция if_7_yes_no. Принимает число. Если оно меньше 7, то вернуть “Yes”, если больше 10, то вернуть “No”,
    если равно 9, то вернуть “Error”.
    TODO А что вернуть, если число == 7, 8 или 10? По идее, мы всегда возвращаем строку (yes, no, error), а тут
     не вернем вообще ничего, то для программиста, который использует нашу функцию, это станет "неожиданным поведением".
     Какие-то bad design practice получаются.
    """
    if number == 9:
        return "Error"

    if number < 7:
        return "Yes"

    if number > 10:
        return "No"
    # Когда ничего не возвращаешь, то на самом деле возвращаешь <class 'NoneType'> (т.е. None)


def check_plus_last_num(number: int) -> int:
    """
    8. Функция check_plus_last_num. Принимает двузначное число. Вернуть результат вычисления выражения:
    число + (последняя цифра числа).
    """
    if number < 10 or number > 99:
        raise Exception("Parameter can be double digit only")

    result = number + int(str(number)[-1])
    return result


def print_welcome(n: int, text: str = "You are welcome!") -> None:
    """
    Функция print_welcome. Принимает число n. Выведите на экран n раз фразу "You are welcome!".
    """
    for i in range(n):
        print(text)


def print_numbers_step1(n: int, start: int = 1, step: int = 1) -> None:
    """
    2. Функция print_numbers_step1. Принимает число n. Выведите на экран числа от 1 до n (включительно) с шагом 1.
    """
    for i in range(start, n + 1, step):
        print(str(i) + ' ', end='')


def count_div_12(a: int, b: int) -> int:
    """
    3. Функция count_div_12. Принимает целые числа a и b. Вернуть количество целых чисел от a до b включительно,
    которые делятся на 12.
    # TODO Опять же, наверное имеется ввиду "делятся нацело на 12" (без остатка)
    """
    count = 0
    for i in range(a, b + 1, 1):
        if i % 12 == 0:
            count += 1
    return count


def count_user_numbers() -> int:
    """
    4. Функция count_user_numbers. Пользователь вводит ненулевые числа до тех пор пока не введет ноль.
    Верните сумму этих чисел
    """
    # Предположим, что пользователь вводил числа и мы получили их в списке:
    user_input = [1, 5, 9, 10, 0]

    summary = 0
    for number in user_input:
        summary += number

    return summary


def print_sum_3num(n: int) -> List[int] or str:
    """
    5. Функция print_sum_3num. Принимает целое число n. Напечатайте трехзначные числа, сумма цифр которых равна n.
    Если нет ни одного такого, то напечатайте “Not found”.
    """
    numbers_found = False
    result_list = []

    for i in range(100, 1000, 1):
        if int(str(i)[0]) + int(str(i)[1]) + int(str(i)[2]) == n:
            result_list.append(i)
            numbers_found = True

    if not numbers_found:
        return "Not found"

    return result_list


def count_even_num(num: int) -> int:
    """
    6. Функция count_even_num. Принимает натуральное число. Верните количество четных цифр в этом числе.
    """
    count = 0
    for i in str(num):
        if int(i) % 2 == 0:
            count += 1
    return count


def print_random_3() -> List[int]:
    """
    7. Функция print_random_3. Вывести 3 случайных числа от 0 до 100 без повторений.
    """
    random_numbers = []
    while len(random_numbers) < 3:
        # Документация на randint говорит: Return random integer in range [a, b], including both end points.
        number = random.randint(0, 100)
        if number not in random_numbers:
            random_numbers.append(number)

    return random_numbers


def check_symb(haystack: str, needle: str) -> str:
    """
    8. Функция check_symb. Принимает строку и символ. Если символ не содержится в строке вывести
    сообщение “Not found!”, иначе “I found it!”.
    """
    if len(needle) > 1:
        raise Exception("Symbol parameter can't be longer then one symbol")

    for c in haystack:
        # TODO Как я понимаю, в питоне == можно заменить на "is", полностью ли они эквивалентны?
        if c is needle:
            return "I found it!"

    return "Not found!"


def triple_sting_print_len(string: str) -> None:
    """
    1. Функция triple_sting_print_len. Принимает строку. Вывести ее три раза через запятую и показать количество
    символов в ней.
    """
    for i in range(3):
        print(string + ', ', end='')
    print(len(string))


def print_3th_symbols(string: str) -> None:
    """
    2. Функция print_3th_symbols. Принимает строку. Вывести третий, шестой, девятый и так далее символы.
    # TODO Возможно, тут надо уточнить, третий, шестой и т.п. - это начиная с нуля или начиная с единицы.
       В бытовом смысле третий символ равен программерскому второму.
       От этого зависит нужно ли в if засовывать i + 1 или просто i.
    """
    for i in range(len(string)):
        if (i + 1) % 3 == 0:
            print(string[i], end='')


def print_diff_count(string1: str, string2: str) -> None:
    """
    3. Функция print_diff_count. Принимает две строки. Вывести большую по длине строку столько раз, на сколько
    символов отличаются строки.
    # TODO А если строки равной длины?
    """
    string_to_print = string1 if len(string1) > len(string2) else string2
    diff = abs(len(string1) - len(string2))

    for i in range(diff):
        print(string_to_print)


def modify_str_if_begin_abc(string: str) -> str:
    """
    4. Функция modify_str_if_begin_abc. Принимает строку. Если она начинается на 'abc', то заменить их на 'www',
    иначе добавить в конец строки 'zzz'. Вернуть результат.
    """
    if string.startswith("abc"):
        string = string.replace("abc", "www", 1)
    else:
        string = string + "zzz"
    return string


def replace_word(string: str) -> str:
    """
    5. Функция replace_word. Принимает строку. Возвращает строку, в которой вхождения 'word' заменены на 'letter'.
    # TODO А не попробовать ли нам реализовать str.replace самостоятельно, разбирая строку по символам?
    """
    return string.replace("word", "letter")


def count_aba(string: str) -> int:
    """
    6. Функция count_aba. Принимает строку. Возвращает количество вхождения 'aba' в строку.
    # TODO А уж не посчитать ли нам эти вхождения без str.count?
    """
    return string.count("aba")


def remove_exclamation(string: str) -> str:
    """
    7. Функция remove_exclamation. Удалите в строке все символы "!". Верните результат.
    # TODO А не удалить ли нам "!" из строки без str.replace?
    """
    return string.replace("!", "")


def count_words_by_spaces(string: str) -> int:
    """
    8. Функция count_words_by_spaces. Принимает строку, состоящую из слов, разделенных пробелами.
    Возвращает количество слов в строке.
    """
    # TODO Заменить str.replace на регулярные выражения, разобраться как они работают в Python.
    string = string.replace("?", "")
    string = string.replace("!", "")
    string = string.replace(",", "")
    string = string.replace(".", "")
    return ' '.join(string.split()).count(" ") + 1


def arithmetic(a, b, operation):
    """
    1. Функция arithmetic. Принимает 3 аргумента: первые 2 - числа, третий - операция, которая должна
    быть произведена над ними. Вернуть результат операции. Если третий аргумент +, сложить их; если
    —, то вычесть; * — умножить; / — разделить (первое на второе). В остальных случаях вернуть строку
    "Неизвестная операция"
    """
    accepted_operations = ['+', '-', '*', '/']
    if operation not in accepted_operations:
        raise Exception("Operation is not allowed")
    




# TODO В Python нормально класть тестовый класс в конце файла с функциями?
#  Java или PHP это неправильно из-за стандарта "один файл = один класс" и тесты всегда лежат отдельно.
class TestLecture2TasksPdf(unittest.TestCase):
    def test_season_logic(self):
        # TODO Можно ли это записать как-то короче на Python (не обязательно используя списки)?
        #  Чтобы не делать миллион assertEquals.
        self.assertListEqual(["Winter", "Winter", "Winter"], [season(12), season(1), season(2)])
        self.assertListEqual(["Spring", "Spring", "Spring"], [season(3), season(4), season(5)])
        self.assertListEqual(["Summer", "Summer", "Summer"], [season(6), season(7), season(8)])
        self.assertListEqual(["Autumn", "Autumn", "Autumn"], [season(9), season(10), season(11)])

    def test_season_exception_15_month(self):
        with self.assertRaises(Exception):
            season(15)

    def test_season_exception_negative_month(self):
        with self.assertRaises(Exception):
            season(13)


if __name__ == '__main__':
    unittest.main()
