from lesson_2.homework.lib.lib import *


# FUNCTIONS ################################################################################################

# SEASON
month = 5
print("Season of month {} is {}.".format(month, season(month)))

# SWAP v1 (function prints result itself)
swap_v1(10, 20)

# SWAP v2 (with lists)
values = [10, 20]
print(f"Swap v2: {values} => {swap_v2(values)}")

# SWAP v3 (with varargs as parameter, but list returned)
a = 10
b = 20
print(f"Swap v3: {a}, {b} => {swap_v3(a, b)[0]}, {swap_v3(a, b)[1]}")

# SWAP v4 (pass arguments as list with *)
result = swap_v4(*[1, 2])
print(f"Swap v4: [1, 2] => {result}")

# CHECK NUMBER BY 5
a = 1
b = 2
c = 8
print(f"Check number by 5 on values a == {a}, b == {b}, c == {c}, \
result is \"{str(check_numbers_by_5(a, b, c)).lower()}\"")

# CHECK EQUAL
a = 5
b = 2
c = 1
print(f"Check equals for {a}, {b}, {c} is {str(check_equal(a, b, c)).lower()}")

# IF 7 YES NO
a = 11
print(f"If 7 yes / no of argument {a}: {type(if_7_yes_no(a))}")

# CHECK PLUS LAST NUM
a = 39
print(f"Check plus last num of {a}: {check_plus_last_num(a)}")

# LOOPS ################################################################################################

# PRINT WELCOME
print("Print welcome:")
print_welcome(2)

# PRINT NUMBERS STEP 1
print("Print numbers step 1:")
print_numbers_step1(5)
# Печать переноса строки
print()

# COUNT DIV 12
print("Count div 12:")
print("{}".format(count_div_12(1, 100)))

# COUNT USER NUMBERS
print("Count user numbers:")
print(f"{count_user_numbers()}")

# PRINT SUM 3 NUM
print("Print sum 3 num:")
print(f"{print_sum_3num(25)}")

# COUNT EVEN NUM
print("Count even num:")
print(f"{count_even_num(163819)}")

# PRINT RANDOM 3
print("Print random 3:")
print(f"{print_random_3()}")

# CHECK SYMB
print("Check symb:")
print("{}".format(check_symb("test", "e")))


# STRINGS ###############################################################################################

print("Triple string pring len:")
triple_sting_print_len("triple")

print("print_3th_symbols:")
print_3th_symbols("thIs Is StrIng")
print()

print("print_diff_count:")
print_diff_count("string1", "string1__")

print("modify_str_if_begin_abc: ")
print("{}".format(modify_str_if_begin_abc('abc_string')))

print("replace_word: ")
print("{}".format(replace_word("This word is not word now!")))

print("count_aba: ")
print("{}".format(count_aba("test aba aba test aba")))

print("remove_exclamation: ")
print("{}".format(remove_exclamation("There! is! no! more! exclamation!.")))

print("count_words_by_spaces: ")
print("{}".format(count_words_by_spaces(" What if spaces are not inside the string AND   INSIDE      TOO  ?    ")))

